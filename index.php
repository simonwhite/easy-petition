<?php
	require_once("config.php");
	$page_name = "Help our cause today."
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $petition_name; echo " - "; echo $page_name; ?></title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="wrap">
	  <h1><?php echo $petition_name; ?></h1>

			<form action="submit.php" method="post" enctype="multipart/form-data">
			  <p>
			    <label for="fname">First Name</label>
			    	<input type="text" name="fname" id="fname" />
				<br />
    		    <label for="lname">Last Name</label>
	                <input type="text" name="lname" id="lname" />
                <br />
			    <label for="email">Your Email Address</label>
			    <input type="text" name="email" id="email" />
				<br />
		        <label for="private">Enable Privacy*</label>
                <input name="private" type="checkbox" id="private" value="checked"/>
			  </p>
			  <p>
			    <input type="submit" name="submit" id="submit" value="Sign Petition" />
			  </p>
	  </form>
      
      <p><small><i>* Should you enable privacy your surname will be removed from the public lists.</i></small></p>
      <?php 
	  if ($list = "yes") {
	  	$query = "SELECT * FROM `".$db_name."`.`Vote` WHERE `Privacy` = \"false\" LIMIT 20";
		$list = mysql_query($query);
			// Check for Zero Rows
			if(mysql_num_rows($list) == 0 ) {
				// Echo no results.
					echo '<p>No votes available.</p>';
					// Results
			} else {
				$vote = mysql_query("SELECT * FROM `".$db_name."`.`Vote`"); 
				$votes = mysql_num_rows($vote);
	 			  echo 'So far we have '.$votes.' sign up.';
					echo '<ul>';
						while($lis = mysql_fetch_array($list))
						{
							// Store Member Details
						 	$fname = $lis['First Name'];
						 	$lname = $lis['Last Name'];
							echo '<li>'.$fname.' '.$lname.'</li>';
						}
			}
	  } else { /* Do nothing  */ }
	  ?>
	</div>
</body>
</html>
